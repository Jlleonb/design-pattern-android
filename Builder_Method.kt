interface Builder {
  fun setNumberSpaces (spaces:Int);
  fun setNumberFloors (floor:Int);
  fun setNumberEntrance(entrances:Int)
  fun setNumberExit (exits:Int)
  fun setCostPerHour(price:Double)
}

class ConcreteBuilder: Builder {

  private var parkingLot = ParkingLot()

  fun reset(){
    parkingLot = ParkingLot()
  }

  override fun setNumberSpaces (spaces:Int){
    parkingLot.spaces = spaces
  }

  override fun setNumberFloors (floor:Int){
    parkingLot.floors = floor
  }

  override fun setNumberEntrance (entrances:Int){
    parkingLot.entrances = entrances
  }

  override fun setNumberExit (exits:Int){
    parkingLot.exits = exits
  }

  override fun setCostPerHour (price:Double){
    parkingLot.costPerHour = price
  }

  fun retrieveParkingLot(): ParkingLot{
    val result = parkingLot
    reset()
    return result
  }

}

class ParkingLot {
    var spaces:Int = 25
    var floors:Int = 1
    var entrances:Int = 1
    var exits:Int = 1
    var costPerHour:Double = 1.0

    fun showParkingLot():String {
      var result = "Parking Lot: \nNumber of Spaces: ${spaces} \nNumber of Floors: ${floors} \nNumber of Entrances: ${entrances} \nNumber of exits: ${exits} \nCost per Hour: ${costPerHour} \n"
      return result
    }

}

fun main (){
  val builder = ConcreteBuilder()
  builder.setNumberSpaces(50)
  builder.setNumberFloors(2)
  builder.setNumberEntrance(3)
  builder.setNumberExit(3)
  builder.setCostPerHour(1.50)
  print (builder.retrieveParkingLot().showParkingLot())
  print ("\nNew Parking Lot 2\n")
  builder.setNumberSpaces(1000)
  builder.setCostPerHour(2.00)
  print (builder.retrieveParkingLot().showParkingLot())
}
