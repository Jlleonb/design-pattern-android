import java.text.SimpleDateFormat
import java.util.Date

object Time {

  fun getTime() : String {
    val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
    val currentDate = sdf.format(Date())
    return currentDate
  }
  
}


fun main(){
  val time = Time.getTime()
  print ("The time is: $time")
}
