interface InterfaceSuscriber {
  fun update(announcement:String)
}

open class Publisher {
  var suscribers = mutableListOf<Suscriber>()

  open fun suscribe(suscriber : Suscriber){
    suscribers.add(suscriber)
  }

  open fun unsuscribe(suscriber:Suscriber){
    suscribers.remove(suscriber)
  }

  open fun publishChange(announcement: String){
    for(s in suscribers){
      s.update(announcement)
    }
  }

}

class NewProduct : Publisher() {

  override fun suscribe(suscriber : Suscriber){
    super.suscribe(suscriber)
  }

  override fun unsuscribe(suscriber : Suscriber){
    super.unsuscribe(suscriber)
  }

  override fun publishChange(announcement: String){
    for (s in suscribers)
      s.update("The new product is: " + announcement + "\n")
  }

}

class NewFood : Publisher() {

  override fun suscribe(suscriber : Suscriber){
    super.suscribe(suscriber)
  }

  override fun unsuscribe(suscriber : Suscriber){
    super.unsuscribe(suscriber)
  }

  override fun publishChange(announcement: String){
    for (s in suscribers)
      s.update("The new food is: " + announcement + "\n")
  }

}

class Suscriber(name:String) : InterfaceSuscriber {

  private var name:String = name

  override fun update(announcement:String){
    print("My name is: "+ name + "  " + announcement)
  }

}

fun main (){
  val suscriber1 = Suscriber("Pedro")
  val suscriber2 = Suscriber("Ana")
  val suscriber3 = Suscriber("Jose")
  val publisher1 = NewProduct()
  val publisher = NewFood()

  publisher1.suscribe(suscriber1)
  publisher1.suscribe(suscriber3)
  publisher.suscribe(suscriber2)
  publisher1.publishChange("Pencil")
  publisher.publishChange("Hamburguer")

  print("\n\n")

  publisher1.unsuscribe(suscriber3)
  publisher.suscribe(suscriber3)

  publisher1.publishChange("Mouse")
  publisher.publishChange("Pizza")
}
